import {
  LAYOUT_NAV_LEFT_OPEN,
  LAYOUT_NAV_LEFT_CLOSE,
  LAYOUT_NAV_RIGHT_OPEN,
  LAYOUT_NAV_RIGHT_CLOSE } from 'constants';

export function toggleLeftNav(isVisible=false){
  console.log(isVisible)
  if(isVisible){
    return { type: LAYOUT_NAV_LEFT_OPEN };
  }else{
    return { type: LAYOUT_NAV_LEFT_CLOSE };
  }
}

export function toggleRightNav(isVisible=false){
  console.log(isVisible)
  if(isVisible){
    return { type: LAYOUT_NAV_RIGHT_OPEN };
  }else{
    return { type: LAYOUT_NAV_RIGHT_CLOSE };
  }
}



