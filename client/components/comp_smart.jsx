import React, { Component } from 'react';
import { connect } from 'react-redux';

class CompSmart extends Component {

  static propTypes = {
    thing: React.PropTypes.string,
    dispatch: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) {

  }

  componentWillMount() {

  }

  componentWillUnmount() {

  }

  componentDidMount() {

  }

  componentWillUpdate(nextProps, nextState) {

  }

  componentDidUpdate(prevProps, prevState) {

  }

  render() {
    return (
      <p>
        { this.props.thing }
      </p>
    );
  }

}


export default connect((state){
  return {
    thing:state.thing
  };
})(CompSmart);
