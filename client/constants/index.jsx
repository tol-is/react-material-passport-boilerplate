import user from "constants/user";
import layout from "constants/layout";

export default Object.assign({},
  user,
  layout );
