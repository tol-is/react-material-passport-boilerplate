import React, { PropTypes } from 'react';
import Navigation from './navigation';
import DevTools from './devtools';
// import 'scss/main';

const App = ({children}) => {
  return (
    <div>
      <Navigation />
      {children}
      <DevTools />
    </div>
  );
};

App.propTypes = {
  children: PropTypes.object
};

export default App;
