import 'styles.scss';

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, Route, browserHistory } from 'react-router';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';
import injectTapEventPlugin from 'react-tap-event-plugin';

//Needed for onTouchTap
//Can go away when react 1.0 release
//https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();

import configureStore from 'store';
import { createWebRoutes } from 'router';

const initialState = {};
const store = configureStore(initialState, browserHistory);
const routes = createWebRoutes(store);

render(
  <Provider store={store}>
    <Router history={browserHistory}>
      {routes}
    </Router>
  </Provider>, document.getElementById('app'));
