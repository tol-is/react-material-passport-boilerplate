import { combineReducers } from 'redux';
import { routeReducer as router } from 'react-router-redux';

import user from './user';
import layout from './layout';

const rootReducer = combineReducers({
  user,
  layout,
  router
});

export default rootReducer;
