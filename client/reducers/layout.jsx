 import {
  LAYOUT_NAV_LEFT_OPEN,
  LAYOUT_NAV_LEFT_CLOSE,
  LAYOUT_NAV_RIGHT_OPEN,
  LAYOUT_NAV_RIGHT_CLOSE } from 'constants';

const initialState = {
  leftNavOpen: false,
  rightNavOpen: false
};

export default function layout( state=initialState, action={} ) {

  console.log(action);

  switch (action.type) {

    case LAYOUT_NAV_LEFT_OPEN:
      console.log('open');
      return Object.asign({}, state, {
        leftNavOpen: true
      });

    case LAYOUT_NAV_LEFT_CLOSE:
      console.log('close');
      return Object.asign({}, state, {
        leftNavOpen: false
      });

    case LAYOUT_NAV_RIGHT_OPEN:
      return Object.asign({}, state, {
        rightNavOpen: true
      });

    case LAYOUT_NAV_RIGHT_CLOSE:
      return Object.asign({}, state, {
        rightNavOpen: false
      });

    default:
      return state;
  }
}
