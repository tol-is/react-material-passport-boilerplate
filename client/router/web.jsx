import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from 'containers/app';
import Welcome from 'containers/welcome';
import Material from 'containers/material';

export default (store) => {

  return (
    <Route path="/" component={App}>
      <IndexRoute component={Welcome} />
      <Route path="material" component={Material}/>
    </Route> );
};
