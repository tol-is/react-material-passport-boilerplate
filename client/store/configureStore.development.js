import { createStore, applyMiddleware, compose } from 'redux';
import { persistState } from 'redux-devtools';
import { syncHistory, routerMiddleware } from 'react-router-redux';
import promiseMiddleware from 'redux-promise-middleware';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';

import rootReducer from 'reducers';
import DevTools from '../containers/devtools';

const logger = createLogger({
  level: 'info',
  collapsed: true,
});

export default function configureStore(initialState, history) {

  const router = routerMiddleware(history);
  // const router = syncHistory(history);

  const enhancer = compose(
    applyMiddleware(thunkMiddleware, promiseMiddleware, router, logger),
    DevTools.instrument(),
    persistState(
      window.location.href.match(
        /[?&]debug_session=([^&]+)\b/
      )
    )
  );
  const store = createStore(rootReducer, initialState, enhancer);

  if (module.hot) {
    module.hot.accept('reducers', () =>
      store.replaceReducer(require('reducers'))
    );
  }

  return store;
}
