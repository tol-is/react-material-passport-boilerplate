import { createStore, applyMiddleware, compose } from 'redux';
import { syncHistory, routerMiddleware } from 'react-router-redux';
import promiseMiddleware from 'redux-promise-middleware';
import thunkMiddleware from 'redux-thunk';

import rootReducer from 'reducers';

export default function configureStore(initialState, history) {

  const router = routerMiddleware(history);
  // const router = syncHistory(history);

  const enhancer = compose(
    applyMiddleware(thunkMiddleware, promiseMiddleware, router)
  );

  const store = createStore(rootReducer, initialState, enhancer);

  return store;
}
