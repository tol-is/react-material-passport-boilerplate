let environment = process.env.NODE_ENV || 'development';
if (environment === 'production') {
  module.exports = require('./configureStore.production');
} else {
  module.exports = require('./configureStore.development');
}
