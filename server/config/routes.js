var auth = require('../controllers/auth');
var user = require('../controllers/user');

module.exports = function(app, passport) {
    app.get('/api/user/logout', user.logout);
    app.get('/api/user', auth.checkForUser);

    // // Social Login Callbacks
    app.get('/auth/facebook', auth.facebook);
    app.get('/auth/facebook/callback', auth.facebookCallback);

}
