var path = require('path');
var webpack = require('webpack');
var autoprefixer = require('autoprefixer');

var assetsPath = path.join(__dirname, '..', 'public', 'assets');
var hotMiddlewareScript = 'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true';
var ExtractTextPlugin = require("extract-text-webpack-plugin");


module.exports = {
    devtool: 'eval',

    name: 'browser',

    entry: {
      client_web: ['index.web', hotMiddlewareScript],
    },

    output: {
      path: assetsPath,
      filename: '[name].js',
      publicPath: '/assets/'
    },

    module: {
        noParse: [],

        loaders: [
        {
            test: /\.js$|\.jsx$/,
            exclude: /node_modules/,
            loaders: ['babel']
        },
        {
            test: /\.(jpg|gif|png)?$/,
            exclude: /node_modules/,
            loader: 'file-loader'
        },
        {
            test: /\.(svg|eot|svg|ttf|woff|woff2)$/,
            exclude: /node_modules/,
            loader: 'file'
        },
        {
            test: /\.(css|scss|sass)?$/,
            loader: ExtractTextPlugin.extract(
                'css!' +
                'postcss-loader!' +
                'sass?sourceMap&outputStyle=expanded'
            )
        }

      ]
    },


    resolve: {
      root: __dirname,

      extensions: ['', '.js', '.jsx', '.scss'],

      modulesDirectories: [
        'node_modules', 'client'
      ],

      alias: {

      }

    },

    plugins: [

        new webpack.HotModuleReplacementPlugin(),

        new webpack.NoErrorsPlugin(),

        new ExtractTextPlugin('styles.css')
    ],

    postcss: function () {
        return [autoprefixer];
    }
};
